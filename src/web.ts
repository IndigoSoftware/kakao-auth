import { WebPlugin } from '@capacitor/core';
import { KakaoAuthPlugin } from './definitions';

export class KakaoAuthWeb extends WebPlugin implements KakaoAuthPlugin {
  constructor() {
    super({
      name: 'KakaoAuth',
      platforms: ['web'],
    });
  }

// 우리가 모델링한 함수의 실제 메서드를 정의합니다.
// 웹에서는 카카오톡 로그인 기능을 넣지 않을 예정이라 console.warn만 출력해줍니다.
  async login() : Promise<any>
  {
    console.warn("No operation on web platform.");
    return false;
  }
}

const KakaoAuth = new KakaoAuthWeb();

export { KakaoAuth };

import { registerWebPlugin } from '@capacitor/core';
registerWebPlugin(KakaoAuth);
