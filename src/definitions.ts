declare module '@capacitor/core' {
  interface PluginRegistry {
    KakaoAuth: KakaoAuthPlugin;
  }
}
// 사용할 메서드의 인터페이스를 정의합니다.
export interface KakaoAuthPlugin {
  login(): Promise<any>; // login 메서드는 웹에서 할 게 없기 떄문에 Promise 형태의 모든 타입을 리턴할 수 있도록 하겠습니다.
}
